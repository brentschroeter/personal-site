import lume from 'lume/mod.ts';
import multilanguage from 'lume/plugins/multilanguage.ts';
import toml from 'lume/plugins/toml.ts';

const site = lume();
site.use(toml());
site.use(multilanguage({
  defaultLanguage: 'en',
  languages: ['de', 'en', 'es'],
}));
site.copy('fonts');
site.copy('images');
site.copy('styles');
site.copy('robots.txt');
site.copy('favicon.ico');

export default site;
